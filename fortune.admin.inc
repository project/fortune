<?php
/**
 * @file
 * Implement the settings form for bsd fortune
 */

require_once(_fortune_module_path() .'/fortune.index.inc');

/**
 * The settings form for uploading new fortunes
 */
function fortune_admin_settings() {
  $checkboxes = _fortune_checkboxes();

  // Customize the checkboxes for the admin form
  $ftu =& $checkboxes['fortunes_to_use'];
  $use_title = $ftu['#title'];
  unset($ftu['#title']);
  $ftu['#description'] = '<br style="clear:both" />'. t('Choose all the fortunes you want to enable in the displayed block.');
  if (user_access('administer blocks')) {
    $ftu['#description'] .= t(' Configure the BSD Fortune block at the !path', array('!path' => '<a href="'. url('admin/build/block') .'">Blocks Admin Page</a>'));
  }

  $ftd = $checkboxes['fortunes_to_use'];
  unset($ftd['#title']);
  unset($ftd['#default_value']);
  unset($ftd['#element_validation']);
  unset($ftd['#options']['fortunes']);
  $ftd['#description'] = '<br style="clear:both" />'. t('Select one or more fortunes to delete from the fortunes folder.');

  return array(
    '#attributes' => array('enctype' => "multipart/form-data"),
    '#validate' => array('_fortune_validate_fortune', '_fortunes_to_use_validate'),

    'fortune_use_field' => array(
      '#title' => $use_title,
      '#type' => 'fieldset',
      'fortunes_to_use' => $ftu,
    ),

    'fortune_upload_field' => array(
      '#title' => t('Add New Fortune'),
      '#type' => 'fieldset',
      '#description' => t('Upload a fortune file to add it to the list. An index file will be created automatically.'),
      'fortune_upload' => array(
        '#type' => 'file',
        '#size' => 30,
      ),
    ),

    'fortune_delete_field' => array(
      '#title' => t('Delete Fortunes?'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'fortunes_to_delete' => $ftd,
    ),

    'fortune_more_field' => array(
      '#title' => t('Get More Fortunes'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'gettable_fortunes' => <<<HTML
      <ul>
        <li><a target="_blank" href="http://www.shlomifish.org/humour/fortunes/">Shlomi Fish's Fortune Collection</a></li>
        <li><a target="_blank" href="http://fortunes.cat-v.org/">The Jar of Fortune Files</a></li>
        <li><a target="_blank" href="http://fortunes.pbworks.com/">The "All Original Unoffensive Fortunes" Wiki</a></li>
      </ul>
HTML;
    ),

    'submit' => array('#type' => 'submit', '#value' => t('Submit')),
  );
}

function fortune_admin_settings_submit($form, &$form_state) {
  // Delete any fortunes so-marked
  $del_items = $form_state['values']['fortunes_to_delete'];
  foreach($del_items as $f=>$yes) {
    if ($yes !== 0) {
      file_delete(_fortunes_dir() ."/$f");
      file_delete(_fortunes_dir() ."/$f.dat");
      drupal_set_message( t('%file Deleted', array('%file'=>ucwords($f))) );
      $deleted[$f] = 1;
    }
  }

  // Remove all deleted items from the enabled list
  $keep = array();
  $enabled = 0;
  foreach($form_state['values']['fortunes_to_use'] as $f=>$v) {
    if (!isset($deleted[$f])) {
      $keep[$f] = $v;
      if ($v !== 0) {
        $enabled++;
      }
    }
  }

  // If none are left enabled then at least enable 'fortunes'
  // (which as a failsafe can't be deleted).
  if (!$enabled) {
    $keep['fortunes'] = 'fortunes';
  }

  $form_state['values']['fortunes_to_use'] = $keep;

  // Update the enabled fortunes
  _fortune_save_fortunes_to_use($keep);
}

function _fortune_validate_fortune(&$form, &$form_state) {
  // If required, validate the uploaded fortune.
  $validators = array(
    'file_validate_size' => array(variable_get('fortune_file_size', 100) * 1024)
  );
  // copy the uploaded file to the site /tmp folder
  if ($file = file_save_upload('fortune_upload', $validators)) {
    $srcpath = $file->filepath;
    $dstname = preg_replace('/\.$/', '', $file->filename);
    if (preg_match('/\.dat$/', $dstname)) {
      form_set_error('fortune_upload', t("Don't upload the index file; just upload the fortune file itself."));
    }
    else {
      $dstname = preg_replace('/\.txt$/', '', $dstname);
      $destination = _fortunes_dir() ."/$dstname";
      if (file_copy($srcpath, $destination, FILE_EXISTS_REPLACE)) {
        if ($err = _fortune_create_index($destination)) {
          form_set_error('fortune_upload', t($err));
          file_delete($destination);
        }
        else {
          drupal_set_message(t('The Fortune file %file was successfully uploaded and indexed.', array('%file'=>$dstname)));
        }
      }
      else {
        form_set_error('fortune_upload', t("Failed to upload the fortune; the 'fortunes' directory doesn't exist or is not writable."));
      }
    }
  }
}

