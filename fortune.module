<?php
/**
 * @file
 * Implement a bsd fortune block
 */

/**
 * Implementation of hook_perm().
 */
function fortune_perm() {
  return array('administer fortune');
}

/**
 * Implementation of hook_init().
 */
function fortune_init() {
  if (preg_match('#admin/build/block/configure/fortune|admin/settings/fortune#', $_GET['q'])) {
    drupal_add_css(drupal_get_path('module','fortune') . '/fortune-admin.css', 'module');
  }
}

/**
 * Implementation of hook_menu().
 */
function fortune_menu() {

  _fortune_update_random_table();

  $items['admin/settings/fortune'] = array(
    'title' => 'BSD Fortune',
    'description' => 'Manage fortune files and enabled fortunes.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fortune_admin_settings'),
    'access arguments' => array('administer fortune'),
    'file' => 'fortune.admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_block().
 */
function fortune_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks['fortune'] = array(
        'info'       => t('BSD Fortune'),
        'cache'      => BLOCK_NO_CACHE,
        'visibility' => 0,
        'region'     => 'left',
      );
      return $blocks;
    case 'configure':
      if ($delta == 'fortune') {
        return _fortune_checkboxes();
      }
	  return;
    case 'save':
      if ($delta == 'fortune' && user_access('administer fortune')) {
        _fortune_save_fortunes_to_use($edit['fortunes_to_use']);
      }
      return;
    case 'view':
      if ($delta == 'fortune') {
        $block = array('subject' => '', 'content' => _fortune_get_block());
      }
      return $block;
  }
}

/**
 * The fortune block!
 */
function _fortune_get_block() {
  drupal_add_css(drupal_get_path('module','fortune') . '/fortune.css', 'module');
  $fortune_text = _fortune_fetch_random_fortune();
  return "<div class=\"fortune-text\">". $fortune_text ."</div>";
}

/**
 * Checkboxes for the block and admin forms
 */
function _fortune_checkboxes() {

  $filenames = _all_fortune_files();
  if (count($filenames)) {

    foreach ($filenames as $fname) {
	    $ftitle = preg_replace('/(.+)-o$/', '<em style="color:red">\1 (offensive!)</em>', ucwords($fname));
      $options[$fname] = $ftitle;
    }

    if (user_access('administer fortune')) {
      $field = array(
        '#title' => t('Enabled Fortunes'),
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => variable_get('fortune_fortunes_list', array_keys($options)),
        '#element_validation' => array('_fortunes_to_use_validate'),
        '#attributes' => array('class' => 'fortune-checkboxes'),
        '#description' => '<br style="clear:both" />'. t('Choose all the fortunes you want to use in this block. Manage fortune files at the !path', array('!path' => '<a href="'. url('admin/settings/fortune') .'">BSD Fortune Admin Page</a>')),
      );
      $form['fortunes_to_use'] = $field;
    }
    else {
      $enabled_items = array();
      foreach(variable_get('fortune_fortunes_list', array()) as $k) {
        $enabled_items[] = $options[$k];
      }
      $form = array(
        'fortunes_to_use' => array(
          '#value' => '<strong>Enabled Fortunes:</strong> ' . implode(', ', $enabled_items),
          )
      );
    }
  }
  else {
    $form = array(
      'fortunes_to_use' => array(
        '#value' => t('No fortunes could be found! Make sure you have the Fortune module installed properly.'),
        )
    );
  }

  return $form;
}

function _fortunes_to_use_validate($form, &$form_state) {
  foreach($form_state['values']['fortunes_to_use'] as $k=>$v) {
    if ($v !== 0) {
      return;
    }
  }
  form_set_error('fortunes_to_use', t('You need to select at least one fortune file! (Changes not saved.)'));
}

/**
 * Save the enabled checkbox states and update the random index
 */
function _fortune_save_fortunes_to_use($field) {
  $fortune_results = array();
  foreach($field as $k=>$v) {
    if ($v !== 0) {
      $fortune_results[] = $k;
    }
  }
  variable_set('fortune_fortunes_list', $fortune_results);
  _fortune_update_random_table();
}

/**
 * Update the random index table
 */
function _fortune_update_random_table() {
  $fortune_files = variable_get('fortune_fortunes_list', array());
  if (count($fortune_files) == 0) {
    $filenames = _all_fortune_files();
    if (count($filenames)) {
      foreach ($filenames as $fname) {
        $fortune_files[] = $fname;
      }
    }
  }

  // Set up a smart randomizer based on the total count
  $total = 0;
  $random_table = array();
  foreach($fortune_files as $fortune) {
    $count = _fortune_get_fortune_count(_fortunes_dir() ."/$fortune.dat");
    $total += $count;
    $random_table[$fortune] = $total;
  }

  variable_set('fortune_random_table', $random_table);
  variable_set('fortune_random_total', $total);
}

/**
 *  Get the basename of all fortune indexes in the fortunes folder
 */
function _all_fortune_files() {
  $fortune_files = array();
  if ( _fortune_test_install() && $handle = opendir(_fortunes_dir()) ) {
    while (false !== ($filename = readdir($handle))) {
      if (preg_match('/(.*)(\.dat)/', $filename, $m)) {
        $fortune_files[] = $m[1];
      }
    }
  }
  return $fortune_files;
}

/**
 *  Test for the existence of the fortunes folder
 */
function _fortune_test_install() {
  return file_exists(_fortunes_dir());
}
/**
 *  Get the full path to the fortunes folder
 */
function _fortunes_dir() {
  return variable_get('fortune_fortunes_dir', file_create_path('fortunes'));
}
/**
 *  Get the full path to this module
 */
function _fortune_module_path() {
  return $_SERVER['DOCUMENT_ROOT'] . '/' . drupal_get_path('module','fortune');
}


/**
 * Get a random fortune using an even distribution method
 */
function _fortune_fetch_random_fortune() {
  $rand_total = variable_get('fortune_random_total', 0);
  $rand_table = variable_get('fortune_random_table', array());
  $count = count($rand_table);
  if ($rand_total && $count) {
    srand((double)microtime()*1000000);
    $rand = rand(0, $rand_total-1);
    $pt = 0;
    foreach($rand_table as $f => $t) {
      if ($rand < $t) {
        $fortune_file = $f;
        $item_index = $rand - $pt;
        break;
      }
      $pt = $t;
    }
    $fortune_text = _fortune_get_fortune_at_index(_fortunes_dir() ."/$fortune_file.dat", $item_index);
  }
  return $fortune_text ? _fortune_detab($fortune_text) : 'Confucius say "He who properly install fortune soon find wisdom."';
}

/*
 * Fortune File Parsing Functions
 */

/**
 * Get the number of fortunes in the given dat file
 */
function _fortune_get_fortune_count($path) {
  $fd = fopen($path, 'rb');
  fseek($fd, 4);
  $count = _fortune_get_long($fd);
  fclose($fd);
  return $count;
}

/**
 * Get a random fortune based on a dat filepath
 */
function _fortune_read_random_fortune($path) {
  return _fortune_get_fortune_at_index($path, rand(0, _fortune_get_fortune_count($path) - 1));
}	

/**
 * Get the fortune at the given index in the given file
 */
function _fortune_get_fortune_at_index($path, $index) {
	if (is_file($path) === FALSE) {
		drupal_set_message("No fortune file found at $path", 'error');
		return;
	}

  $fd = fopen($path, 'rb');
	if ($fd !== FALSE) {
  	fseek($fd, 24 + 4 * $index);
  	$offset = _fortune_get_long($fd);
  	fclose($fd);

  	$fortune_path = substr($path, 0, strlen($path) - 4);

  	if ( ($fd = fopen($fortune_path, 'rb')) == FALSE ) {
  		drupal_set_message("Can't open fortune file: $fortune_path!", 'error');
  	}

  	$out = _fortune_read_fortune($fd, $offset);
  	fclose($fd);
  	return $out;
  }
	else {
		drupal_set_message("Can't open fortune index file: $path", 'error');
  }
}

/**
 * Get the fortune at the given offset in the file
 */
function _fortune_read_fortune($fd, $offset) {
  fseek($fd, $offset);
  $line = $fortune = '';
  do {
    while (FALSE !== ($pos = strpos($line, "\t"))) {
      $line = substr($line, 0, $pos) . str_repeat(' ', 8 - $pos % 8) . substr($line, $pos + 1);
    }
	  $fortune .= $line;
    $line = fgets($fd, 1024);
  } while ($line[0] != "%" && !feof($fd));
  return $fortune;
}

/**
 * Get a big-endian long from the file
 */
function _fortune_get_long($fd) {
  $long = fread($fd, 4);
  return ord($long[3]) + (ord($long[2]) << 8) + (ord($long[1]) << 16) + (ord($long[0]) << 24);
}
