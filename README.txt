
*** A FORTUNE BLOCK FOR THE DRUPAL GENERATION ***

This module provides a "BSD Fortune" block that displays a random fortune from a set of fortune files that you supply. Once the module is installed, just go to the blocks administration page and add the "BSD Fortune" block to any page region.

The text in almost all fortune files is formatted for an 80-character-wide display, so the block works best if you put it in a wider region above or below your main content or in the page footer. Shorter fortunes like "disclaimer" may work okay in a sidebar block.

*** CONFIGURING FORTUNE ***

The BSD Fortune module comes with just the original fortune files, which are free for distribution. You can enable just the ones you want to see by clicking the BSD Fortune block's "configure" link, or by using the BSD Fortune Settings form at admin/settings/fortune.

*** MANAGING FORTUNE FILES ***

Fortune files are stored in a folder named "fortunes" in your site's "files" folder. Through FTP (for example) you can drop fortune and .dat files together into this folder and the module will include them.

The simplest way to manage your fortune files is use the BSD Fortune Settings form at admin/settings/fortune. There you can choose which fortunes will be displayed in the block, upload new fortune files, and delete any of the installed fortunes. (Since there needs to be some fortune data the default fortune file "fortunes" can't be deleted here.)

This page also includes links to some online fortune file sources.

More information can be found at Thinkyhead's Fortune Module page...

  http://www.thinkyhead.com/design/fortune-module

Scott Lahteine
Thinkyhead
December 7, 2009
