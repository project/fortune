<?php
/**
 * @file
 * Includes a function like "strfile" for creating index files
 */

/**
 * Function to generate a fortune index file
 * (Ok if fortune begins with % or forgets the last %)
 */
function _fortune_create_index($filepath) {
  $fd = @fopen($filepath, 'r');
  if ($fd === false) { return "Unable to open the uploaded file for indexing."; }

  $offset[] = $length = $longest = 0;
  $shortest = 999999;
  do {
    if (!feof($fd) && "%\n" !== ($line = fgets($fd))) {
      $length += strlen($line);
      continue;
    }
    if ($length > 0) {
      $offset[] = ftell($fd);
      if ($length > $longest)   $longest = $length;
      if ($length < $shortest)  $shortest = $length;
      $length = 0;
    }
  } while (!feof($fd));

  fclose($fd);

  if (count($offset) < 5) { return "Unable to index the uploaded file. Make sure the file is formatted properly."; }

  $fd = @fopen("$filepath.dat", 'w');
  if ($fd === FALSE) { return "Unable to create the index file."; }

  // Write a standard bsd fortune header
  _fortune_chr4($fd, 2);                // version 2
  _fortune_chr4($fd, count($offset));   // number of indices
  _fortune_chr4($fd, $longest);         // the largest fortune size
  _fortune_chr4($fd, $shortest);        // the smallest fortune size
  _fortune_chr4($fd, 0);                // flags: plaintext
  _fortune_chr4($fd, ord('%') << 24);   // bitpacked delimiter (% assumed)

  for ($i=0; $i<count($offset); $i++) {
    _fortune_chr4($fd, $offset[$i]);
  }

  fclose($fd);

  return FALSE;
}

// helper to writes out a CHR4
function _fortune_chr4($fd, $l) {
  fwrite($fd, chr(($l>>24)&0xFF).chr(($l>>16)&0xFF).chr(($l>>8)&0xFF).chr($l&0xFF));
}
